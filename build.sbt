name := """silex-eventsourced"""
organization := "hu.silex"

version := "1.1.8"

lazy val root = project in file(".")

scalaVersion := "2.12.3"

scalacOptions ++= Seq("-feature", "-deprecation")

libraryDependencies ++= Seq(
  "io.jvm.uuid" % "scala-uuid_2.12" % "0.2.3",
  "com.geteventstore" %% "eventstore-client" % "4.1.1",
  "com.typesafe.play" % "play-json_2.12" % "2.6.3",

  "org.scalatest" % "scalatest_2.12" % "3.0.4" % Test
)

fork in Test := false

coverageMinimum := 80
coverageFailOnMinimum := true