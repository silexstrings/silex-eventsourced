package hu.silex.eventsourced.ops

import eventstore.{ClassTags, EsConnection, In, Out, UserCredentials}

import scala.concurrent.{ExecutionContext, Future}

trait EventStoreConnection {
  def apply[OUT <: Out, IN <: In](out: OUT, credentials: Option[UserCredentials] = None)(
    implicit
    outIn: ClassTags[OUT, IN],
    ec:    ExecutionContext
  ): Future[IN]

  def connection[T]:T
}

class EventStoreConnectionImpl(es: EsConnection) extends EventStoreConnection {
  def apply[OUT <: Out, IN <: In](out: OUT, credentials: Option[UserCredentials] = None)(
    implicit
    outIn: ClassTags[OUT, IN],
    ec:    ExecutionContext
  ): Future[IN] = es(out)

  override def connection[T]:T = es.asInstanceOf[T]
}
