package hu.silex.eventsourced.ops

trait ValidCommands[T <: models.EventSourced[T]] {
  val commandMapper: models.Aggregate[T] => PartialFunction[models.AggregateCommand, models.AggregateEvent]
}

object ValidCommands {

  def apply[T <: models.EventSourced[T]](
    f: models.Aggregate[T] => PartialFunction[models.AggregateCommand, models.AggregateEvent]
  ):ValidCommands[T] = new ValidCommands[T] {

    override val commandMapper: models.Aggregate[T] => PartialFunction[models.AggregateCommand, models.AggregateEvent] = f

  }
}
