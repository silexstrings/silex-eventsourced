package hu.silex.eventsourced.ops

import play.api.libs.json.JsValue

trait ValidEvents[T <: models.EventSourced[T]] {
  val json2AggregateEvent: PartialFunction[models.AggregateEventType, JsValue => models.AggregateEvent]
  val aggregateEvent2EventData: PartialFunction[models.AggregateEvent, JsValue]
}

object ValidEvents {

  def apply[T <: models.EventSourced[T]](
    fromEs:PartialFunction[models.AggregateEventType, JsValue => models.AggregateEvent]
  )(toEs: PartialFunction[models.AggregateEvent, JsValue]):ValidEvents[T] = new ValidEvents[T] {

    override val json2AggregateEvent: PartialFunction[models.AggregateEventType, JsValue => models.AggregateEvent] = fromEs
    override val aggregateEvent2EventData: PartialFunction[models.AggregateEvent, JsValue] = toEs
  }
}
