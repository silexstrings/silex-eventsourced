package hu.silex.eventsourced.ops.enrichments

import hu.silex.eventsourced.ops.ValidCommands
import hu.silex.eventsourced.ops.models._

import scala.util.Try


trait Commandable[T <: EventSourced[T]] {
  def verify(command: AggregateCommand)(implicit ev:ValidCommands[T]):AggregateEvent

  def apply(command: AggregateCommand)(implicit ev:ValidCommands[T]):Aggregate[T]

  def apply(commands: AggregateCommand*)(implicit ev:ValidCommands[T]):Aggregate[T]
}

trait CommandableOps {

  implicit class AggregateIsCommandable[T <: EventSourced[T]](aggregate: Aggregate[T]) extends Commandable[T] {

    override def verify(command: AggregateCommand)(implicit ev:ValidCommands[T]):AggregateEvent = {
      val f = ev.commandMapper(aggregate)
      if (f.isDefinedAt(command)) {
        f(command)
      }
      else {
        throw AggregateException.UnsupportedCommand(command)
      }
    }

    override def apply(command:AggregateCommand)(implicit ev:ValidCommands[T]):Aggregate[T] = Try(verify(command)).fold(
        error => throw error,
        event => aggregate.appended(event)
      )


    override def apply(commands: AggregateCommand*)(implicit ev: ValidCommands[T]): Aggregate[T] =
      commands.foldLeft(aggregate){ (aggregate, command) => aggregate.apply(command) }
  }
}
