package hu.silex.eventsourced.ops.enrichments

import eventstore.{Content, EventData, WriteEvents}
import hu.silex.eventsourced.ops.models.{Aggregate, EventSourced}
import hu.silex.eventsourced.ops.{EventStoreConnection, ValidEvents}

import scala.concurrent.{ExecutionContext, Future}

trait Persistable[T <: EventSourced[T]] {
  def persist()(implicit ev: ValidEvents[T]):Future[Aggregate[T]]
}

trait PersistableOps {

  implicit class AggregateIsPersistable[T <: EventSourced[T]](aggregate: Aggregate[T])(
    implicit
    es: EventStoreConnection,
    ex: ExecutionContext
  ) extends Persistable[T] {

    override def persist()(implicit ev: ValidEvents[T]): Future[Aggregate[T]] = {
      val persistable:List[EventData] = aggregate.memoryEvents.collect {
        case event if ev.aggregateEvent2EventData.isDefinedAt(event) =>
          EventData(
            eventType = event.eventType.eventType,
            data = Content.Json(ev.aggregateEvent2EventData(event).toString())
          )
      }

      for {
        persisted <- es(WriteEvents(aggregate.stream, persistable))
      } yield aggregate.allEventsPersisted()
    }

  }
}
