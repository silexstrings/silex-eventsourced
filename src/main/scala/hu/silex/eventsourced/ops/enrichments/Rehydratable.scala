package hu.silex.eventsourced.ops.enrichments

import eventstore.{Content, ReadStreamEvents, StreamNotFoundException}
import hu.silex.eventsourced.ops.models._
import hu.silex.eventsourced.ops.{EventStoreConnection, ValidCommands, ValidEvents}
import play.api.libs.json.Json

import scala.concurrent.{ExecutionContext, Future}
import scala.reflect._

trait Rehydratable[T <: EventSourced[T]] {
  def rehydrate(createCommand:AggregateCommand)(implicit ev:ValidEvents[T], vc: ValidCommands[T]):Future[Aggregate[T]]
  def rehydrate(createCommand: Option[AggregateCommand] = None)(implicit ev:ValidEvents[T], vc: ValidCommands[T]):Future[Aggregate[T]]
}

trait RehydratableOps {

  implicit class AggregateIsRehydratable[T <: EventSourced[T]](aggregate: Aggregate[T])(
    implicit
    es: EventStoreConnection,
    ex: ExecutionContext,
    ct: ClassTag[T]
  ) extends Rehydratable[T] {

    override def rehydrate(createCommand: AggregateCommand)(implicit ev: ValidEvents[T], vc: ValidCommands[T]): Future[Aggregate[T]] = {
      rehydrate(Some(createCommand))
    }

    override def rehydrate(createCommand: Option[AggregateCommand] = None)(implicit ev:ValidEvents[T], vc: ValidCommands[T]):Future[Aggregate[T]] = (for {
      readEvents <- es(ReadStreamEvents(aggregate.stream))
    } yield readEvents.events.foldLeft(aggregate){ (applied, event) =>
      val jsonContent = event.data.data match {
        case Content.Json(json) => Json.parse(json)
        case _ => throw AggregateException.UnsupportedConentType()
      }
      val eventType = AggregateEventType(event.data.eventType)
      if (ev.json2AggregateEvent.isDefinedAt(eventType)) {
        applied.appendedPersistedEvent(ev.json2AggregateEvent(eventType)(jsonContent))
      }
      else {
        throw AggregateException.UnsupportedEvent(AggregateEventType(event.data.eventType), jsonContent)
      }
    })
      .recover {
        case ex:StreamNotFoundException if createCommand.isDefined =>
          val res = aggregate.copy(justCreated = true)(aggregate.factory).appended(aggregate.verify(createCommand.get))
          res
        case ex:StreamNotFoundException =>
          throw AggregateException.NotCreatedYet()
      }

  }
}
