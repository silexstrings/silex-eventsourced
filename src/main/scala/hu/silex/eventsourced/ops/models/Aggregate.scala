package hu.silex.eventsourced.ops.models

import eventstore.EventStream
import io.jvm.uuid._

import scala.reflect._

case class Aggregate[T <: EventSourced[T]](
  aggregateId: UUID,
  view: Option[T] = None,
  justCreated:Boolean = false
)(val factory: () => T)(implicit ct: ClassTag[T]) { self =>

  private def viewWithDefaults():T = factory()

  private[ops] val memoryEvents:List[AggregateEvent] = List()
  private[ops] val persistedEvents:List[AggregateEvent] = List()

  private[ops] lazy val stream:EventStream.Id = EventStream.Id(ct.runtimeClass.getName + "-" + aggregateId)

  private[ops] def appendedPersistedEvent(event: AggregateEvent):Aggregate[T] = new Aggregate[T] (
    aggregateId = self.aggregateId,
    view = (self.view orElse Some(viewWithDefaults())).map(_.handle(event))
  )(self.factory) {
    override val memoryEvents: List[AggregateEvent] = self.memoryEvents
    override val persistedEvents: List[AggregateEvent] = self.persistedEvents :+ event
  }

  def appended(event: AggregateEvent):Aggregate[T] = new Aggregate[T](
    aggregateId = self.aggregateId,
    view = (self.view orElse Some(viewWithDefaults())).map(_.handle(event)),
    justCreated = self.justCreated
  )(self.factory) {
    override val memoryEvents: List[AggregateEvent] = self.memoryEvents :+ event
    override val persistedEvents: List[AggregateEvent] = self.persistedEvents
  }

  private[ops] def allEventsPersisted():Aggregate[T] = new Aggregate[T](
    aggregateId = self.aggregateId,
    view = self.view orElse Some(viewWithDefaults())
  )(self.factory) {
    override val memoryEvents: List[AggregateEvent] = List()
    override val persistedEvents: List[AggregateEvent] = self.events
  }

  lazy val events = persistedEvents ++ memoryEvents

  def get():T = view getOrElse viewWithDefaults()
}
