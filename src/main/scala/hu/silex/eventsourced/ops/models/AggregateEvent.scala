package hu.silex.eventsourced.ops.models

trait AggregateEvent {
  val eventType:AggregateEventType
}

case class AggregateEventType(eventType: String) extends AnyVal
