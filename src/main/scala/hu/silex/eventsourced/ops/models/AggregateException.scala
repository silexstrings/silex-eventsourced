package hu.silex.eventsourced.ops.models

import play.api.libs.json.JsValue

trait AggregateException extends Exception {

  case class UnsupportedConentType() extends AggregateException
  case class UnsupportedEvent(eventType: AggregateEventType, eventData: JsValue) extends AggregateException
  case class UnsupportedCommand(command: AggregateCommand) extends AggregateException

  case class AlreadyExists() extends AggregateException
  case class NotCreatedYet() extends AggregateException
}

object AggregateException extends AggregateException

