package hu.silex.eventsourced.ops.models

trait EventSourced[T <: EventSourced[T]] { this:T =>
  def handle(aggregateEvent: AggregateEvent):T
}
