package hu.silex.eventsourced

import hu.silex.eventsourced.ops.enrichments._
import io.jvm.uuid._

import scala.reflect._

package object ops extends RehydratableOps with CommandableOps with PersistableOps {
  type Aggregate[T <: models.EventSourced[T]] = models.Aggregate[T]

  type AggregateEvent = models.AggregateEvent
  type AggregateEventType = models.AggregateEventType

  type AggregateCommand = models.AggregateCommand

  type EventSourced[T <: EventSourced[T]] = models.EventSourced[T]

  type AggregateException = models.AggregateException

  object AggregateEventType {
    def apply(eventType: String):AggregateEventType = models.AggregateEventType(eventType)
  }

  object Aggregate {
    class AggregateBuilder[T <: models.EventSourced[T]](val factory: () => T) {
      def withId(aggregateId: UUID)(implicit ct: ClassTag[T]):models.Aggregate[T] = models.Aggregate[T](aggregateId)(factory)
    }

    def apply[T <: models.EventSourced[T] : ClassTag](factory:() => T):AggregateBuilder[T] =
      new AggregateBuilder[T](factory)

  }

}
