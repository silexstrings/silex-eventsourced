package hu.silex.eventsourced.test

import eventstore._
import hu.silex.eventsourced.ops.EventStoreConnection

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

/*
 Please note that this is not a fully operational fake, it only implements what this test suite needs

  * connection(ReadStreamEvents(streamId)) -> ReadStreamEventsCompleted
  * connection(WriteEvents(streamId, events)) -> WriteEventsCompleted (with no meaningful response data)
 */
class FakeEventStoreConnection extends EventStoreConnection {
  import FakeEventStoreConnection._

  override def apply[OUT <: Out, IN <: In](out: OUT, credentials: Option[UserCredentials] = None)(
    implicit
    outIn: ClassTags[OUT, IN],
    ec:    ExecutionContext
  ): Future[IN] = out match {
    case ReadStreamEvents(streamId, fromNumber, maxCount, direction, resolveLinkTos, requireMaster) =>
      //only streamId is properly handled
      Try {
        val events = streams(streamId)
        Future.successful(ReadStreamEventsCompleted(
          events = events,
          nextEventNumber = EventNumber.Exact(events.size), //unused
          lastEventNumber = EventNumber.Exact(events.size), //unused
          endOfStream = true, //unused
          lastCommitPosition = events.size.toLong, //unused
          direction = direction //unused
        )).mapTo[IN](outIn.in)
      }
      .recover {
        case ex:NoSuchElementException => Future.failed(eventstore.StreamNotFoundException(streamId))
      }
      .get
    case WriteEvents(streamId, events, _, _) =>
      val oldEvents = streams.getOrElse(streamId, List.empty[Event])
      streams += streamId -> (oldEvents ++ events.zipWithIndex.map {
        case (data, index) => EventRecord(
          streamId = streamId,
          number = EventNumber.Exact(oldEvents.size + index),
          data = data
        )
      })
      Future.successful(WriteEventsCompleted()).mapTo[IN](outIn.in)
  }

  override def connection[T]:T = null.asInstanceOf[T] //scalastyle:ignore null

}

object FakeEventStoreConnection {
  private val streams = mutable.Map[EventStream.Id, List[Event]]()
}

trait WithFakeEventStore {
  implicit val es:EventStoreConnection = new FakeEventStoreConnection()
}
