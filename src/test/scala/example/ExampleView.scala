package example

import hu.silex.eventsourced.ops._
import hu.silex.eventsourced.ops.models.AggregateException
import play.api.libs.json.{JsNull, Json}

case class ExampleView (
  data: Option[String] = None
) extends EventSourced[ExampleView] {

  override def handle(aggregateEvent: AggregateEvent): ExampleView = aggregateEvent match {
    case ExampleView.Created(baseData) =>
      copy(data = Some(baseData))
    case ExampleView.DataChanged(newData) =>
      copy(data = Some(newData))
    case ExampleView.Deleted =>
      copy(data = None)
    case _ =>
      this
  }

}

object ExampleView {

  case class Created(data:String) extends AggregateEvent {
    override val eventType: AggregateEventType = ExampleView.Created.eventType
  }

  object Created {
    val eventType: AggregateEventType = AggregateEventType("example_created")
    implicit val format = Json.format[ExampleView.Created]
  }

  case class DataChanged(newData:String) extends AggregateEvent {
    override val eventType: AggregateEventType = ExampleView.DataChanged.eventType
  }

  object DataChanged {
    val eventType = AggregateEventType("example_datachanged")
    implicit val format = Json.format[ExampleView.DataChanged]
  }

  case object Deleted extends AggregateEvent {
    override val eventType: AggregateEventType = AggregateEventType("exampled_deleted")
  }

  implicit val validEvents = ValidEvents[ExampleView] {
    case Created.eventType => _.as[Created]
    case DataChanged.eventType => _.as[DataChanged]
    case Deleted.eventType => _ => Deleted
  }{
    case event:Created => Json.toJson(event)
    case event:DataChanged => Json.toJson(event)
    case Deleted => JsNull
  }

  case class Create(initialData:Option[String]) extends AggregateCommand
  case class Update(data: String) extends AggregateCommand
  case object Delete extends AggregateCommand

  implicit val validCommands = ValidCommands[ExampleView] { aggregate =>
    {
      case Create(data) =>
        if (aggregate.events.isEmpty) {
          Created(data.getOrElse(""))
        }
        else {
          throw AggregateException.AlreadyExists()
        }
      case Update(data) =>
        if (aggregate.events.exists(_.isInstanceOf[Created])) {
          DataChanged(data)
        }
        else {
          throw AggregateException.NotCreatedYet()
        }
      case Delete =>
        if (!aggregate.events.exists(_.isInstanceOf[Created])) {
          throw AggregateException.NotCreatedYet()
        }
        else {
          Deleted
        }
    }
  }
}
