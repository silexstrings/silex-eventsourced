package helpers

import org.scalatest.{MustMatchers, OptionValues, WordSpec}

trait UnitSpec extends WordSpec with MustMatchers with OptionValues
