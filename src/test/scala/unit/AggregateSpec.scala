package unit

import example.ExampleView
import helpers.UnitSpec
import hu.silex.eventsourced.ops._
import io.jvm.uuid._

class AggregateSpec extends UnitSpec {

  "an eventsourced object" must {

    "be properly creatable from an eventsourced case class" in {
      val id = UUID.random
      val exampleAggregate = Aggregate(() => ExampleView()).withId(id)

      exampleAggregate.aggregateId mustEqual id
      exampleAggregate.events mustBe empty
      exampleAggregate.get() mustBe an[ExampleView]
    }

    "store a new event" in {
      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random)
      val event = ExampleView.Created("")

      val appended = aggregate.appended(event)

      aggregate.events mustBe empty
      appended.events must contain only event
    }

    "store new events in order" in {
      val event1 = ExampleView.Created("")
      val event2 = ExampleView.DataChanged("new data")
      val event3 = ExampleView.DataChanged("newer data")

      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random)
        .appended(event1)
        .appended(event2)
        .appended(event3)

      aggregate.events must contain inOrderOnly (event1, event2, event3)
    }

    "represent events in the contained view according to its handle() function" in {
      val event = ExampleView.Created("data")

      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random).appended(event)

      aggregate.get().data.value mustEqual event.data
    }
  }

}
