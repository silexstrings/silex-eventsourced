package unit

import example.ExampleView
import example.ExampleView.{Created, Update}
import helpers.UnitSpec
import hu.silex.eventsourced.ops._
import hu.silex.eventsourced.ops.models.AggregateException
import io.jvm.uuid._

class CommandableSpec extends UnitSpec {

  "An aggregate's commands" must {

    "throw an exception when an invalid command is being verified" in {
      val command = Update("new data")
      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random)

      a[AggregateException.NotCreatedYet] must be thrownBy aggregate.verify(command)
    }

    "throw an exception when an invalid command is being applied" in {
      val command = Update("new data")
      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random)

      a[AggregateException.NotCreatedYet] must be thrownBy aggregate.apply(command)
    }

    "generate the appropriate event when a valid command is being verified" in {
      val command = Update("new data")
      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random).appended(Created(""))

      aggregate.verify(command) mustEqual ExampleView.DataChanged(command.data)
    }

    "generate the updated aggregate when a valid command is being applied" in {
      val id = UUID.random
      val command = Update("new data")
      val aggregate = Aggregate(() => ExampleView()).withId(id).appended(Created(""))

      val result = aggregate.apply(command)

      result.aggregateId mustEqual id
      result.events must contain(ExampleView.DataChanged(command.data))
    }

    "throw an exception when a command belonging to another aggregate is verified" in {
      case class OtherCreate() extends AggregateCommand
      val command = OtherCreate()

      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random)
      an[AggregateException.UnsupportedCommand] must be thrownBy aggregate.verify(command)
    }

    "throw an exception when a command belonging to another aggregate is applied" in {
      case class OtherCreate() extends AggregateCommand
      val command = OtherCreate()

      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random)
      an[AggregateException.UnsupportedCommand] must be thrownBy aggregate.apply(command)
    }

    "generate the updated aggregate when multiple valid commands are being applied" in {
      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random)
      val commands = Seq(
        ExampleView.Create(None),
        ExampleView.Update("updated")
      )

      val result = aggregate.apply(commands: _*)

      result.get().data.value mustEqual "updated"
    }

    "throw an exception when multiple commands are being applied and some of them are invalid" in {
      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random)
      val commands = Seq(
        ExampleView.Create(None),
        ExampleView.Update("updated"),
        ExampleView.Create(None)
      )

      an[AggregateException.AlreadyExists] must be thrownBy aggregate.apply(commands: _*)
    }

    "throw an exception when multiple commands are being applied and some of them are belonging to another aggregate" in {
      case class OtherCreate() extends AggregateCommand
      val command = OtherCreate()
      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random)

      val commands = Seq(
        ExampleView.Create(None),
        ExampleView.Update("updated"),
        OtherCreate()
      )

      an[AggregateException.UnsupportedCommand] must be thrownBy aggregate.apply(commands: _*)
    }

  }

}
