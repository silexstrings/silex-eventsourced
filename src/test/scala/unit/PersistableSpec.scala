package unit

import example.ExampleView
import helpers.UnitSpec
import hu.silex.eventsourced.ops._
import hu.silex.eventsourced.test.WithFakeEventStore
import io.jvm.uuid._

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps

class PersistableSpec extends UnitSpec with WithFakeEventStore {

  "an aggregate when persisting" must {

    "respond with the same view as persisted" in {
      val event = ExampleView.Created("")
      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random).appended(event)

      val result = Await.result(aggregate.persist(), 5 seconds)

      result mustEqual aggregate
      result.events mustEqual aggregate.events
    }

    "respond with the same view as persisted for multiple events" in {
      val event1 = ExampleView.Created("")
      val event2 = ExampleView.DataChanged("data")
      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random).appended(event1).appended(event2)

      val result = Await.result(aggregate.persist(), 5 seconds)

      result mustEqual aggregate
      result.events mustEqual aggregate.events
    }

  }

}
