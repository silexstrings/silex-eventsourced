package unit

import example.ExampleView
import helpers.UnitSpec
import hu.silex.eventsourced.ops._
import hu.silex.eventsourced.ops.models.AggregateException
import hu.silex.eventsourced.test.WithFakeEventStore
import io.jvm.uuid._

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps

class RehydratableSpec extends UnitSpec with WithFakeEventStore {

  "an aggregate when rehydrating" must {

    "rehydrate with zero events when a default create command is specified" in {
      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random)
      val rehydrated = Await.result(aggregate.rehydrate(ExampleView.Create(None)), 5 seconds)

      aggregate.events mustBe empty
      rehydrated.events must contain only aggregate.verify(ExampleView.Create(None))
      rehydrated.justCreated mustBe true
    }

    "not rehydrate with zero events when a default create command is not specified" in {
      val aggregate = Aggregate(() => ExampleView()).withId(UUID.random)
      a[AggregateException.NotCreatedYet] must be thrownBy Await.result(aggregate.rehydrate(), 5 seconds)
    }

    "rehydrate with already persisted events" in {
      //build up
      val id = UUID.random
      val event1 = ExampleView.Created("")
      val aggregate = Aggregate(() => ExampleView()).withId(id).appended(event1)
      val persisted = Await.result(aggregate.persist(), 5 seconds)

      //rehydration from clean
      val other = Aggregate(() => ExampleView()).withId(id)
      val rehydrated = Await.result(other.rehydrate(), 5 seconds)

      other.events mustBe empty
      rehydrated.events mustEqual aggregate.events
      rehydrated mustEqual aggregate
      rehydrated.events mustEqual persisted.events
      rehydrated mustEqual persisted
      rehydrated.justCreated mustBe false
    }

    "able to append events in the proper order after rehydration" in {
      //build up
      val id = UUID.random
      val event1 = ExampleView.Created("")
      val aggregate = Aggregate(() => ExampleView()).withId(id).appended(event1)
      val persisted = Await.result(aggregate.persist(), 5 seconds)

      //rehydration from clean
      val other = Aggregate(() => ExampleView()).withId(id)
      val event2 = ExampleView.DataChanged("data")
      val rehydrated = Await.result(other.rehydrate(), 5 seconds).appended(event2)

      rehydrated.events must contain inOrderOnly(event1, event2)
      rehydrated.justCreated mustBe false
    }

    "able to append events in the proper order after rehydration with default creation when existing" in {
      //build up
      val id = UUID.random
      val event1 = ExampleView.Created("")
      val aggregate = Aggregate(() => ExampleView()).withId(id).appended(event1)
      val persisted = Await.result(aggregate.persist(), 5 seconds)

      //rehydration from clean
      val other = Aggregate(() => ExampleView()).withId(id)
      val event2 = ExampleView.DataChanged("data")
      val rehydrated = Await.result(other.rehydrate(ExampleView.Create(None)), 5 seconds).appended(event2)

      rehydrated.events must contain inOrderOnly(event1, event2)

      rehydrated.justCreated mustBe false
    }

    "able to append events in the proper order after rehydration with default creation when nonexistent" in {
      //rehydration from clean
      val other = Aggregate(() => ExampleView()).withId(UUID.random)
      val event2 = ExampleView.DataChanged("data")
      val rehydrated = Await.result(other.rehydrate(ExampleView.Create(None)), 5 seconds).appended(event2)

      rehydrated.events must contain inOrderOnly(other.verify(ExampleView.Create(None)), event2)
      rehydrated.justCreated mustBe true
    }
  }
}
